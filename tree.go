package tot

import (
	"net/http"
	"strings"
)

// node 节点
type node struct {
	path      string       // 路径
	index     string       // 索引
	wildChild bool         // 是否野孩子
	priority  uint32       // 优先级
	children  []*node      // 子节点
	handler   http.Handler // 处理器
}

// incrementChildPriority 增加孩子节点优先级
func (n *node) incrementChildPriority(position int) (newPosition int) {
	current := n
	currentChildren := current.children
	currentChildren[position].priority++
	priority := currentChildren[position].priority
	for newPosition = position; newPosition > 0 && currentChildren[newPosition-1].priority < priority; newPosition-- {
		currentChildren[newPosition-1], currentChildren[newPosition] = currentChildren[newPosition], currentChildren[newPosition-1]
	}
	if newPosition != position {
		current.index = current.index[:newPosition] + current.index[position:position+1] + current.index[newPosition:position] + current.index[position+1:]
	}
	return
}

// insert 插入节点
func (n *node) insert(path string, handler http.Handler) {
	current := n
	fullPath := path
	current.priority++
	if current.path == "" && current.index == "" {
		current.path = path
		current.handler = handler
		return
	}
walk:
	for {
		i, pL, cPL := 0, len(path), len(current.path)
		max := pL
		if pL > cPL {
			max = cPL
		}
		for i < max && path[i] == current.path[i] {
			i++
		}
		if i < cPL {
			child := &node{
				path:      current.path[i:],
				wildChild: current.wildChild,
				index:     current.index,
				children:  current.children,
				handler:   current.handler,
				priority:  current.priority - 1,
			}
			current.children = []*node{child}
			current.index = string(current.path[i])
			current.path = path[:i]
			current.handler = nil
			current.wildChild = false
		}
		if i < pL {
			path = path[i:]
			if current.wildChild {
				current = current.children[0]
				current.priority++
				if len(path) >= len(current.path) && current.path == path[:len(current.path)] && (len(current.path) >= len(path) || path[len(current.path)] == '/') {
					continue walk
				} else {
					pathSeg := path
					prefix := fullPath[:strings.Index(fullPath, pathSeg)] + n.path
					panic("'" + pathSeg + "' in new path '" + fullPath + "' conflicts with existing wildcard '" + n.path + "' in existing prefix '" + prefix + "'")
				}
			}
			index := path[0]
			if index == '/' && len(current.children) == 1 {
				current = current.children[0]
				current.priority++
				continue walk
			}
			for _, v := range []byte(current.index) {
				if v == index {
					k := current.incrementChildPriority(i)
					current = current.children[k]
					continue walk
				}
			}
			current.index += string(index)
			child := &node{}
			current.children = append(current.children, child)
			current.incrementChildPriority(len(current.index) - 1)
			current = child
			current.path = path
			current.handler = handler
			return
		}
		if current.handler != nil {
			panic("a handle is already registered for path '" + fullPath + "'")
		}
		current.handler = handler
		return
	}
}

// search 查询节点
func (n *node) search(path string) (handler http.Handler) {
	current := n
walk:
	for {
		prefix := current.path
		if len(path) > len(prefix) {
			if path[:len(prefix)] == prefix {
				path = path[len(prefix):]
				if !current.wildChild {
					index := path[0]
					for k, v := range []byte(current.index) {
						if v == index {
							current = current.children[k]
							continue walk
						}
					}
					return
				}
				current = current.children[0]
				handler = current.handler
				return
			}
		} else if path == prefix {
			handler = current.handler
		}
		return
	}
}
