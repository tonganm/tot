package tot

import (
	"context"
	"encoding/json"
	"net/http"

	"google.golang.org/grpc/metadata"
)

// FinalizerFunction 终结方法，请求响应数据解析
type FinalizerFunction func(ctx context.Context, w http.ResponseWriter, r *http.Request)

// RequestFunction 请求方法，请求数据解析
type RequestFunction func(ctx context.Context, r *http.Request) (request interface{}, err error)

// ResponseFunction 响应方法，响应数据解析
type ResponseFunction func(ctx context.Context, w http.ResponseWriter, response interface{}) (err error)

// BeforeFunction 请求前方法，请求前处理
type BeforeFunction func(beforeCtx context.Context, r *http.Request) (ctx context.Context)

// AfterFunction 请求后方法，请求后处理
type AfterFunction func(beforeCtx context.Context, w http.ResponseWriter) (ctx context.Context)

// ErrorFunction 错误方法，错误信息
type ErrorFunction func(ctx context.Context, w http.ResponseWriter, err error)

// GRPCFinalizerFunction 终结方法，请求响应数据解析
type GRPCFinalizerFunction func(ctx context.Context, err error)

// GRPCRequestFunction 请求方法，请求数据解析
type GRPCRequestFunction func(ctx context.Context, r interface{}) (request interface{}, err error)

// GRPCResponseFunction 响应方法，响应数据解析
type GRPCResponseFunction func(ctx context.Context, w interface{}) (response interface{}, err error)

// GRPCBeforeFunction 请求前方法，请求前处理
type GRPCBeforeFunction func(ctx context.Context, md metadata.MD) (retCtx context.Context)

// GRPCAfterFunction 请求后方法，请求后处理
type GRPCAfterFunction func(ctx context.Context, header *metadata.MD, trailer *metadata.MD) (retCtx context.Context)

// defaultErrorFunction 默认错误方法
func defaultErrorFunction(ctx context.Context, w http.ResponseWriter, err error) {
	contentType, body := "text/plain; charset=utf-8", []byte(err.Error())
	if marshaller, ok := err.(json.Marshaler); ok {
		if jsonBody, marshalErr := marshaller.MarshalJSON(); marshalErr == nil {
			contentType, body = "application/json; charset=utf-8", jsonBody
		}
	}
	w.Header().Set("Content-Type", contentType)
	code := http.StatusInternalServerError
	w.WriteHeader(code)
	_, _ = w.Write(body)
}
