package tot

// HandlerOption 处理器操作
type HandlerOption func(handler *Handler)

// GRPCHandlerOption 处理器操作
type GRPCHandlerOption func(handler *GRPCHandler)

// FinalizerOption 终结操作
func FinalizerOption(finalizer ...FinalizerFunction) (handler HandlerOption) {
	handler = func(handler *Handler) {
		handler.finalizer = append(handler.finalizer, finalizer...)
	}
	return
}

// BeforeOption 处理前操作
func BeforeOption(before ...BeforeFunction) (handler HandlerOption) {
	handler = func(handler *Handler) {
		handler.before = append(handler.before, before...)
	}
	return
}

// AfterOption 处理后操作
func AfterOption(after ...AfterFunction) (handler HandlerOption) {
	handler = func(handler *Handler) {
		handler.after = append(handler.after, after...)
	}
	return
}

// ErrorOption 错误操作
func ErrorOption(error ErrorFunction) (handler HandlerOption) {
	handler = func(handler *Handler) {
		handler.error = error
	}
	return
}

// GRPCFinalizerOption 终结操作
func GRPCFinalizerOption(finalizer ...GRPCFinalizerFunction) (handler GRPCHandlerOption) {
	handler = func(handler *GRPCHandler) {
		handler.finalizer = append(handler.finalizer, finalizer...)
	}
	return
}

// GRPCBeforeOption 处理前操作
func GRPCBeforeOption(before ...GRPCBeforeFunction) (handler GRPCHandlerOption) {
	handler = func(handler *GRPCHandler) {
		handler.before = append(handler.before, before...)
	}
	return
}

// GRPCAfterOption 处理后操作
func GRPCAfterOption(after ...GRPCAfterFunction) (handler GRPCHandlerOption) {
	handler = func(handler *GRPCHandler) {
		handler.after = append(handler.after, after...)
	}
	return
}
