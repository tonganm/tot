package tot

import (
	"context"
	"net/http"

	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

// Handler 处理器
type Handler struct {
	endpoint  Endpoint            // 端点
	finalizer []FinalizerFunction // 终结器
	request   RequestFunction     // 请求方法
	response  ResponseFunction    // 响应方法
	before    []BeforeFunction    // 请求前方法
	after     []AfterFunction     // 请求后方法
	error     ErrorFunction       // 错误方法
}

// GRPCHandler 处理器
type GRPCHandler struct {
	endpoint  Endpoint                // 端点
	finalizer []GRPCFinalizerFunction // 终结器
	request   GRPCRequestFunction     // 请求方法
	response  GRPCResponseFunction    // 响应方法
	before    []GRPCBeforeFunction    // 请求前方法
	after     []GRPCAfterFunction     // 请求后方法
}

// NewGRPCHandler 新建处理器
func NewGRPCHandler(endpoint Endpoint, request GRPCRequestFunction, response GRPCResponseFunction, option ...GRPCHandlerOption) (handler *GRPCHandler) {
	handler = &GRPCHandler{
		endpoint: endpoint,
		request:  request,
		response: response,
	}
	for i := range option {
		option[i](handler)
	}
	return
}

// NewHandler 新建处理器
func NewHandler(endpoint Endpoint, request RequestFunction, response ResponseFunction, option ...HandlerOption) (handler *Handler) {
	handler = &Handler{
		endpoint: endpoint,
		request:  request,
		response: response,
		error:    defaultErrorFunction,
	}
	for i := range option {
		option[i](handler)
	}
	return
}

// ServeHTTP 实现HTTP处理器
func (h Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	if len(h.finalizer) > 0 {
		defer func(finalizer []FinalizerFunction, ctx context.Context, w http.ResponseWriter, r *http.Request) {
			for i := range finalizer {
				finalizer[i](ctx, w, r)
			}
		}(h.finalizer, ctx, w, r)
	}
	for _, f := range h.before {
		ctx = f(ctx, r)
	}
	request, err := h.request(ctx, r)
	if err != nil {
		h.error(ctx, w, err)
		return
	}
	response, err := h.endpoint(ctx, request)
	if err != nil {
		h.error(ctx, w, err)
		return
	}
	for _, f := range h.after {
		ctx = f(ctx, w)
	}
	err = h.response(ctx, w, response)
	if err != nil {
		h.error(ctx, w, err)
		return
	}
}

// ServeGRPC 实现GRPC处理器
func (g GRPCHandler) ServeGRPC(ctx context.Context, request interface{}) (retCtx context.Context, response interface{}, err error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		md = metadata.MD{}
	}
	if len(g.finalizer) > 0 {
		defer func(finalizer []GRPCFinalizerFunction, ctx context.Context, err error) {
			for i := range finalizer {
				finalizer[i](ctx, err)
			}
		}(g.finalizer, ctx, err)
	}
	for _, f := range g.before {
		ctx = f(ctx, md)
	}
	req, err := g.request(ctx, request)
	if err != nil {
		return ctx, nil, err
	}
	resp, err := g.endpoint(ctx, req)
	if err != nil {
		return ctx, nil, err
	}

	var mdHeader, mdTrailer metadata.MD
	for _, f := range g.after {
		ctx = f(ctx, &mdHeader, &mdTrailer)
	}

	grpcResp, err := g.response(ctx, resp)
	if err != nil {
		return ctx, nil, err
	}

	if len(mdHeader) > 0 {
		if err = grpc.SendHeader(ctx, mdHeader); err != nil {
			return ctx, nil, err
		}
	}

	if len(mdTrailer) > 0 {
		if err = grpc.SetTrailer(ctx, mdTrailer); err != nil {
			return ctx, nil, err
		}
	}

	return ctx, grpcResp, nil
}
