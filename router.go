package tot

import (
	"fmt"
	"net/http"
)

// PanicHandler 错误处理器
type PanicHandler func(w http.ResponseWriter, r *http.Request, err interface{})

// Router 路由器
type Router struct {
	tree         map[string]*node // 前缀树
	NotFound     http.Handler     // 未找到处理器
	PanicHandler PanicHandler     // 错误处理器
}

// NewRouter 创建路由器
func NewRouter() *Router {
	return &Router{}
}

// GET GET处理器
func (r *Router) GET(path string, handler http.Handler) {
	r.Handler(http.MethodGet, path, handler)
}

// HEAD HEAD处理器
func (r *Router) HEAD(path string, handle http.Handler) {
	r.Handler(http.MethodHead, path, handle)
}

// OPTIONS OPTIONS处理器
func (r *Router) OPTIONS(path string, handle http.Handler) {
	r.Handler(http.MethodOptions, path, handle)
}

// POST POST处理器
func (r *Router) POST(path string, handler http.Handler) {
	r.Handler(http.MethodPost, path, handler)
}

// PUT PUT处理器
func (r *Router) PUT(path string, handler http.Handler) {
	r.Handler(http.MethodPut, path, handler)
}

// PATCH PATCH处理器
func (r *Router) PATCH(path string, handler http.Handler) {
	r.Handler(http.MethodPatch, path, handler)
}

// DELETE DELETE处理器
func (r *Router) DELETE(path string, handler http.Handler) {
	r.Handler(http.MethodDelete, path, handler)
}

// Handler 处理器
func (r *Router) Handler(method, path string, handler http.Handler) {
	if method == "" {
		panic("method must not be empty")
	}
	if len(path) < 1 || path[0] != '/' {
		panic("path must begin with '/' in path '" + path + "'")
	}
	if handler == nil {
		panic("handle must not be nil")
	}
	if r.tree == nil {
		r.tree = map[string]*node{}
	}
	root := r.tree[method]
	if root == nil {
		root = &node{}
		r.tree[method] = root
	}
	root.insert(path, handler)
}

// HandlerFunc 处理器
func (r *Router) HandlerFunc(method, path string, handler http.HandlerFunc) {
	r.Handler(method, path, handler)
}

// recover 收集器
func (r *Router) recover(w http.ResponseWriter, req *http.Request) {
	if rcv := recover(); rcv != nil {
		if r.PanicHandler != nil {
			r.PanicHandler(w, req, rcv)
		} else {
			http.Error(w, fmt.Sprintln(rcv), http.StatusInternalServerError)
		}
	}
}

// ServeHTTP 实现HTTP处理器
func (r *Router) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	defer r.recover(w, req)
	path := req.URL.Path
	if root := r.tree[req.Method]; root != nil {
		if handler := root.search(path); handler != nil {
			handler.ServeHTTP(w, req)
			return
		}
	}
	if r.NotFound != nil {
		r.NotFound.ServeHTTP(w, req)
	} else {
		http.NotFound(w, req)
	}
}
