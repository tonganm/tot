package tot

import (
	"context"
)

// Endpoint 端点，传入上下文与请求，返回响应与错误信息
type Endpoint func(ctx context.Context, request interface{}) (response interface{}, err error)

// Middleware 中间件，端点中间件调用
type Middleware func(Endpoint) Endpoint

// Chain 中间件链式调用
func Chain(outer Middleware, others ...Middleware) Middleware {
	return func(next Endpoint) Endpoint {
		for i := len(others) - 1; i >= 0; i-- { // reverse
			next = others[i](next)
		}
		return outer(next)
	}
}
